use crossterm::event::{self, Event, KeyCode};
use ratatui::{backend::Backend, Terminal};
use tui_input::backend::crossterm::EventHandler;

use event_tab::handle_tabs_event;

use crate::prelude::Result;

use super::{
    app::{AppTui, CurrentScreen},
    layout::ui,
};

mod event_tab;

pub async fn run_app<B: Backend>(
    terminal: &mut Terminal<B>,
    app: &mut AppTui,
) -> Result<bool> {
    loop {
        terminal.draw(|f| ui(f, app))?;

        if let Event::Key(key) = event::read()? {
            if key.kind == event::KeyEventKind::Release {
                continue;
            }

            match app.current_screen {
                CurrentScreen::Main => match key.code {
                    KeyCode::Tab => app.current_screen = CurrentScreen::Editing,
                    KeyCode::Enter | KeyCode::Char('S') | KeyCode::Char('s') => app.table.pick(),
                    KeyCode::Char('d') | KeyCode::Char('D') => {
                        if !app.table.picked.is_empty() {
                            app.saved_input.clear();
                            app.load_picked();
                            app.current_screen = CurrentScreen::Download;
                        }
                    }
                    KeyCode::Delete => {
                        if !app.table.picked.is_empty() {
                            app.load_picked();
                            app.current_screen = CurrentScreen::Deleting;
                        }
                    }
                    KeyCode::Char('q') | KeyCode::Char('Q') => {
                        app.current_screen = CurrentScreen::Exiting
                    },
                    KeyCode::Char('j') | KeyCode::Char('J') | KeyCode::Down => app.table.next(),
                    KeyCode::Char('k') | KeyCode::Char('K') | KeyCode::Up => app.table.previous(),
                    _ => {}
                },
                CurrentScreen::Settings => {
                    handle_tabs_event(app, key);
                }
                CurrentScreen::PrepareDownload => match key.code {
                    KeyCode::Char('n') | KeyCode::Char('N') => {
                        app.current_screen = CurrentScreen::Main
                    }
                    KeyCode::Enter | KeyCode::Char('y') | KeyCode::Char('Y') | KeyCode::Esc => {
                        match app.save_input().await {
                            Ok(_) => {
                                app.input_uri.reset();
                                app.save_history()?;
                                return Ok(true);
                            }
                            Err(err) => {
                                app.set_error_msg(err.to_string());
                                app.input_uri.reset();
                            }
                        }
                    }
                    _ => {}
                },
                CurrentScreen::Download => match key.code {
                    KeyCode::Char('y') |
                    KeyCode::Char('Y') | KeyCode::Enter => {
                        return Ok(true);
                    }
                    KeyCode::Char('n') |
                    KeyCode::Char('N') => {
                        app.current_screen = CurrentScreen::Main
                    }
                    KeyCode::Char('q') |
                    KeyCode::Char('Q') => {
                        app.current_screen = CurrentScreen::Exiting
                    }
                    _ => {}
                },
                CurrentScreen::Exiting => match key.code {
                    KeyCode::Char('y') |
                    KeyCode::Char('Y') | KeyCode::Enter => {
                        return Ok(false);
                    }
                    KeyCode::Char('n') |
                    KeyCode::Char('N') |
                    KeyCode::Esc => {
                        app.current_screen = CurrentScreen::Main
                    }
                    _ => (),
                },
                CurrentScreen::Editing => match key.code {
                    KeyCode::Enter => {
                        if !app.input_uri.value().is_empty() {
                            app.clear_saved_input();
                            app.current_screen = CurrentScreen::PrepareDownload;
                        }
                    }
                    KeyCode::Esc => {
                        app.input_uri.reset();
                        app.current_screen = CurrentScreen::Exiting;
                    }
                    KeyCode::Tab => app.current_screen = CurrentScreen::Settings,
                    _ => {
                        app.input_uri.handle_event(&Event::Key(key));
                    }
                },
                // TODO:
                CurrentScreen::Deleting => match key.code {
                    KeyCode::Char('y') | KeyCode::Char('Y') | KeyCode::Enter => {
                        app.print_vec().expect("ERROR");
                        app.current_screen = CurrentScreen::Main;
                        return Ok(false);
                    }
                    KeyCode::Char('n') | KeyCode::Char('N') |
                    KeyCode::Esc => app.current_screen = CurrentScreen::Main,
                    _ => (),
                },
                CurrentScreen::ErrorScreen => match key.code {
                    KeyCode::Enter => {
                        app.clear_error_msg();
                        app.current_screen = CurrentScreen::Main;
                    }
                    _ => {}
                },
            }
        }
    }
}