use std::{
    fs::{self, create_dir_all, File},
    path::PathBuf,
};

use eyre::{eyre, OptionExt};
use indexmap::IndexMap;
use ron::{
    de::from_reader,
    ser::{PrettyConfig, to_string_pretty},
};
use serde::{Deserialize, Serialize};

use crate::prelude::{
    APP_NAME,
    DownloadHistory,
    DownloadState,
    Result,
};

//region Structures
//

#[derive(Debug, Serialize, Deserialize)]
pub struct History(IndexMap<u32, DownloadHistory>);

impl History {
    pub fn new(filename: &str) -> Self {
        let dir_path = Self::check_config_folder()
            .map_err(|err| println!("ERROR : {err}"))
            .expect("ERROR: error creating config folder");

        let file_path = Self::check_config_file(dir_path, filename);

        match file_path {
            Some(file_path) => {
                let file_path = File::open(file_path).expect("ERROR : Opening config file");
                let hist: Self = from_reader(file_path)
                    .map_err(|err| println!("ERROR: {err}"))
                    .unwrap();

                hist
            }
            None => {
                let map_history = IndexMap::new();
                Self(map_history)
            }
        }
    }

    pub fn get_history_by_idx(&self, index: usize) -> Result<(&u32, &DownloadHistory)> {
        let res = self
            .0
            .get_index(index)
            .ok_or_else(|| eyre!("ERROR: No history with index: {index}"))?;

        Ok(res)
    }

    pub fn get_history(&self, index: u32) -> Result<&DownloadHistory> {
        let res = self
            .0
            .get(&index)
            .ok_or_else(|| eyre!("ERROR: No history with key number: {index}"))?;
        Ok(res)
    }

    pub fn update_stage(&mut self, index: u32, state: DownloadState) {
        self.0
            .entry(index)
            .and_modify(|h| h.state = state);
    }

    pub fn add_history(&mut self, download_history: DownloadHistory) -> u32 {
        let last = self
            .0
            .last()
            .and_then(|l| Some(*l.0))
            .unwrap_or_default();

        let key = last + 1;

        self.0.insert(key, download_history);

        key
    }

    pub fn save_history(&self, filename: &str) -> Result<()> {
        let file_path = Self::get_file_history(filename)?;
        let pretty_config = PrettyConfig::new().depth_limit(4).enumerate_arrays(true);
        let pretty_str = to_string_pretty(self, pretty_config)?;

        fs::write(file_path, pretty_str)?;

        Ok(())
    }

    fn remove_history(&mut self, index: usize) -> Option<(u32, DownloadHistory)> {
        self.0.shift_remove_index(index)
    }

    pub fn list(&self) -> &IndexMap<u32, DownloadHistory> {
        &self.0
    }

    //noinspection DuplicatedCode
    fn get_file_history(history_filename: &str) -> Result<PathBuf> {
        let dir_config = dirs::config_dir()
            .ok_or_eyre("ERROR: config directory not available")?;
        let config_file = dir_config.join(APP_NAME).join(history_filename);

        Ok(config_file)
    }

    //noinspection DuplicatedCode
    fn check_config_folder() -> Result<PathBuf> {
        let dir_config = dirs::config_dir()
            .ok_or_eyre("ERROR: config directory not available")?;

        let config_file = dir_config.join(APP_NAME);

        if !config_file.exists() {
            create_dir_all(&config_file)?;
        }

        Ok(config_file)
    }

    fn check_config_file(path: PathBuf, history_filename: &str) -> Option<PathBuf> {
        let file_path = path.join(history_filename);

        let file_path = file_path.exists().then_some(file_path);

        file_path
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }
}

//
//endregion Structures