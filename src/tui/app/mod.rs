use indexmap::IndexMap;
use ratatui::widgets::ListState;
use tui_input::Input;

use table::Table;

use crate::config::{Config, Language};
use crate::prelude::{
    DownloadHistory,
    DownloadState,
    Result,
};
use crate::prelude::*;

use self::{history::History, tabs_state::SelectedTabs};

pub mod history;
pub mod table;
pub mod tabs_state;

// TODO: I prefer to use RON instead of JSON when I develop non-web rust applications =)
pub const HISTORY_FILE_NAME: &str = "history.json";

pub enum CurrentScreen {
    Deleting,
    Download,
    Editing,
    ErrorScreen,
    Exiting,
    Main,
    PrepareDownload,
    Settings,
}

pub enum InputMode {
    Normal,
    Editing,
}

pub struct AppTui {
    pub input_uri: Input,
    // pub input_mode: InputMode,
    pub current_screen: CurrentScreen,
    pub saved_input: Vec<(u32, String)>,
    pub error_msg: String,
    pub history: History,
    pub table: Table,
    pub selected_tabs: SelectedTabs,
    pub settings: Config,
    pub tab_content_input: Input,
    pub tab_content_mode: InputMode,
    pub lang_state: ListState,
}

impl AppTui {
    pub fn new(config_setting: Config) -> Self {
        let hist = History::new(HISTORY_FILE_NAME);
        let hist_len = hist.len();

        Self {
            input_uri: Input::default(),
            current_screen: CurrentScreen::Main,
            saved_input: Vec::new(),
            history: hist,
            table: Table::new(hist_len),
            error_msg: String::new(),
            selected_tabs: SelectedTabs::default(),
            settings: config_setting,
            tab_content_input: Input::default(),
            tab_content_mode: InputMode::Normal,
            lang_state: ListState::default(),
        }
    }

    pub fn next_lang(&mut self) {
        let i = match self.lang_state.selected() {
            Some(i) => {
                if i >= 2 {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };
        self.lang_state.select(Some(i));
    }

    pub fn prev_lang(&mut self) {
        let i = match self.lang_state.selected() {
            Some(i) => {
                if i == 0 {
                    1
                } else {
                    i - 1
                }
            }
            None => 0, // self.last_selected.unwrap_or(0),
        };
        self.lang_state.select(Some(i));
    }

    pub fn update_config(&mut self, input_str: &str, lang: Option<Language>) -> Result<()> {
        match self.selected_tabs {
            SelectedTabs::DownloadFolder => {
                self.settings.update_default_folder(input_str)?;
            }
            SelectedTabs::ConcurrentTotal => {
                self.settings.update_concurrent_downloads(input_str)?;
            }
            SelectedTabs::ChunkSize => {
                self.settings.update_chunk_size(input_str)?;
            }
            SelectedTabs::MinimumSize => {
                self.settings.update_min_size(input_str)?;
            }
            SelectedTabs::Language => {
                if let Some(lang) = lang {
                    self.settings.change_language(lang);
                }
            }
        }

        self.settings.save_history()?;

        Ok(())
    }

    //noinspection DuplicatedCode
    pub fn load_picked(&mut self) {
        let indexes = &self.table.picked;

        indexes.iter().for_each(|idx| {
            let hist = self.history.get_history_by_idx(*idx);
            match hist {
                Ok((num, hist)) => {
                    let url = hist.url();

                    self.saved_input.push((*num, url.to_string()))
                }
                Err(_) => (),
            }
        });

        self.table.picked.clear();
    }

    //noinspection DuplicatedCode
    pub fn delete_picked(&mut self) {
        let indexes = &self.table.picked;

        indexes.iter().for_each(|idx| {
            let hist = self.history.get_history_by_idx(*idx);
            match hist {
                Ok((num, hist)) => {
                    let url = hist.url();

                    self.saved_input.push((*num, url.to_string()))
                }
                Err(_) => (),
            }
        });

        self.table.picked.clear();
    }

    pub async fn save_input(&mut self) -> Result<()> {
        let input_value = self.input_uri.clone();
        let input_value = input_value.value();
        let chunks = self.settings.total_chunks;
        let hist = DownloadHistory::new(input_value, chunks).await?;
        let num = self.add_history(hist);

        self.saved_input.push((num, input_value.to_string()));

        self.input_uri.reset();

        Ok(())
    }

    pub fn print_vec(&self) -> Result<()> {
        let output = serde_json::to_string_pretty(&self.saved_input)?;
        println!("{}", output);
        Ok(())
    }

    pub fn list_history(&self) -> &IndexMap<u32, DownloadHistory> {
        self.history.list()
    }

    pub async fn push_to_table(&mut self) {
        let uri = self.input_uri.value();
        let chunks = self.settings.total_chunks;
        let hist = DownloadHistory::new(uri, chunks).await;

        match hist {
            Ok(hist) => {
                self.add_history(hist);
            }
            Err(err) => self.set_error_msg(err.to_string()),
        }

        self.input_uri.reset();
    }

    pub fn add_history(&mut self, history: DownloadHistory) -> u32 {
        let key = self.history.add_history(history);
        let len = self.history.len();
        self.table.total_len = len;
        key
    }

    pub fn update_stage(&mut self, num: u32, stage: DownloadState) {
        self.history.update_stage(num, stage);
    }

    pub fn get_history(&self, num: u32) -> Result<&DownloadHistory> {
        let res = self.history.get_history(num)?;
        Ok(res)
    }

    pub fn save_history(&self) -> Result<()> {
        self.history.save_history(HISTORY_FILE_NAME)?;
        Ok(())
    }

    pub fn set_error_msg(&mut self, msg: String) {
        self.error_msg = msg;
        self.current_screen = CurrentScreen::ErrorScreen;
    }

    pub fn clear_error_msg(&mut self) {
        self.error_msg = String::new();
    }

    pub fn clear_saved_input(&mut self) {
        self.saved_input.clear();
    }

    pub fn next_tab(&mut self) {
        self.selected_tabs = self.selected_tabs.next();
    }

    pub fn previous_tab(&mut self) {
        self.selected_tabs = self.selected_tabs.prev();
    }
}