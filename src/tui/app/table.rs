use ratatui::{
    style::{Color, palette::tailwind},
    widgets::{ScrollbarState, TableState},
};

const ITEM_HEIGHT: usize = 3;

const PALETTES: [tailwind::Palette; 4] = [
    tailwind::EMERALD,
    tailwind::GRAY,
    tailwind::INDIGO,
    tailwind::RED,
];

pub struct TableColors {
    pub alt_row_color: Color,
    pub buffer_bg: Color,
    pub header_bg: Color,
    pub header_fg: Color,
    pub normal_row_color: Color,
    pub picked_color: Color,
    pub row_fg: Color,
    pub selected_style_fg: Color,
}

impl TableColors {
    const fn new(color: &tailwind::Palette) -> Self {
        Self {
            alt_row_color: tailwind::ZINC.c900,
            buffer_bg: tailwind::ZINC.c950,
            header_bg: color.c900,
            header_fg: tailwind::ZINC.c200,
            normal_row_color: tailwind::ZINC.c950,
            picked_color: tailwind::ROSE.c600,
            row_fg: tailwind::ZINC.c200,
            selected_style_fg: color.c400,
        }
    }
}

pub struct Table {
    pub colors: TableColors,
    pub picked: Vec<usize>,
    pub scroll_state: ScrollbarState,
    pub state: TableState,
    pub total_len: usize,
}

impl Table {
    pub fn new(len: usize) -> Self {
        Self {
            colors: TableColors::new(&PALETTES[0]),
            picked: Vec::new(),
            scroll_state: ScrollbarState::new(len),
            state: TableState::default().with_selected(0),
            total_len: len,
        }
    }

    pub fn next(&mut self) {
        let index = match self.state.selected() {
            Some(i) => {
                if i >= self.total_len - 1 {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };

        self.state.select(Some(index));
        self.scroll_state = self.scroll_state.position(index + ITEM_HEIGHT);
    }

    pub fn previous(&mut self) {
        let index = match self.state.selected() {
            Some(i) => {
                if i == 0 {
                    self.total_len - 1
                } else {
                    i - 1
                }
            }
            None => 0,
        };

        self.state.select(Some(index));
        self.scroll_state = self.scroll_state.position(index + ITEM_HEIGHT);
    }

    pub fn pick(&mut self) {
        match self.state.selected() {
            Some(index) => {
                if let Some(i) = self.picked.iter().position(|val| val == &index) {
                    self.picked.swap_remove(i);
                } else {
                    self.picked.push(index)
                }
            }
            None => {}
        };
    }
}