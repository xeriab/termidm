use ratatui::{
    Frame,
    layout::Rect,
    style::{Color, Style},
    text::{Line, Span},
    widgets::{Block, Borders, BorderType, Padding, Paragraph},
};

use crate::prelude::{
    APP_NAME,
    APP_TITLE,
    APP_VERSION,
    SUB_TITLE,
};

pub fn header_comp(frame: &mut Frame, area: Rect) {
    let width = area.width;
    let height = area.height;
    let title_content = match width {
        width if width > 30 => APP_TITLE,
        _ => APP_NAME,
    };


    let name = Line::from(format!("{}", APP_NAME)).centered();
    let title = Line::from(format!("{}", title_content)).centered();
    let version = Line::from(format!("v{} ", APP_VERSION)).centered();
    let line = Line::from(format!("{}", "------")).style(Style::default().fg(Color::DarkGray)).centered();

    let subtitle = Span::styled(format!("{}", SUB_TITLE), Style::default().fg(Color::Blue));
    let div = Span::styled(" | ", Style::default().fg(Color::DarkGray));
    let author = Span::styled("by Xeriab Nabil", Style::default().fg(Color::Blue));
    let spans = vec![subtitle, div, author];

    let foot = Line::from(spans).centered();

    let para = Paragraph::new(
        vec![
            name,
            title,
            version,
            line,
            foot
        ]
    );

    let y = (height - 6) / 2;

    let title_block = para
        .block(title_outer_block(y))
        .centered()
        .style(Style::default().fg(Color::LightBlue));

    frame.render_widget(title_block, area)
}

fn title_outer_block(num: u16) -> Block<'static> {
    let block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::DarkGray))
        .padding(Padding::vertical(num))
        .border_type(BorderType::Rounded);

    block
}
