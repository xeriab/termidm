use ratatui::{
    style::{Color, Style},
    text::Text,
    widgets::{Block, Borders, BorderType, Paragraph, Wrap},
};

use crate::{l10n::L10N, tui::app::AppTui};

pub fn popup_error(app: &AppTui) -> Paragraph<'static> {
    let language = &app.settings.language;
    let error_popup = L10N::ErrorPopup;

    let popup_exit_component = Block::default()
        .title(error_popup.load(language))
        .borders(Borders::ALL)
        .border_type(BorderType::Rounded)
        .style(Style::default().bg(Color::Red).fg(Color::White));

    let msg = app.error_msg.clone();

    let exit_text = Text::styled(msg, Style::default().fg(Color::Red));
    let exit_paragraph = Paragraph::new(exit_text)
        .block(popup_exit_component)
        .centered()
        .wrap(Wrap { trim: false });

    exit_paragraph
}
