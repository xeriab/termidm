use ratatui::{
    Frame,
    layout::{Alignment, Constraint, Layout, Rect},
    style::{Color, Style},
    text::Line,
    widgets::{Block, block::Title, Borders, BorderType, Paragraph, Wrap},
};

use crate::{l10n::L10N, tui::app::AppTui};

//noinspection DuplicatedCode
pub fn render_download_popup(frame: &mut Frame, app: &mut AppTui, area: Rect) {
    let language = &app.settings.language;
    let title = L10N::DownloadTitle;
    let nav = L10N::DownloadNav;
    let content = L10N::DownloadContent;

    let popup_download_component = Block::default()
        .title(title.load(language))
        .title(Title::from(nav.load(language)).alignment(Alignment::Right))
        .borders(Borders::ALL)
        .border_type(BorderType::Rounded)
        .style(Style::default().fg(Color::LightMagenta));

    let mut download_text = vec![Line::styled(
        content.load(language),
        Style::default().fg(Color::White),
    )];

    for (_, url) in &app.saved_input {
        let url_format = format!(" * {}", url);

        let line_url = Line::styled(url_format, Style::default().fg(Color::Gray));

        download_text.push(line_url);
    }

    let download_paragraph = Paragraph::new(download_text)
        .block(popup_download_component)
        .wrap(Wrap { trim: false });

    frame.render_widget(download_paragraph, area);
}

pub fn render_begin_download(frame: &mut Frame, app: &mut AppTui, area: Rect) {
    let language = &app.settings.language;
    let title = L10N::DownloadPrepareTitle;
    let nav = L10N::DownloadNav;
    let content = L10N::DownloadContent;
    let loading = L10N::DownloadLoading;

    // We can separate/refactor the following into functions
    let popup_download_component = Block::default()
        .title(title.load(language))
        .title(Title::from(nav.load(language)).alignment(Alignment::Right))
        .borders(Borders::ALL)
        .border_type(BorderType::Rounded)
        .style(Style::default().fg(Color::LightYellow));

    frame.render_widget(popup_download_component, area);

    let block_msg = Block::default().borders(Borders::BOTTOM);

    let layout = Layout::vertical([Constraint::Fill(1), Constraint::Length(3)]);
    let [upper_l, bottom_l] = layout.areas(area);

    let input_val = &app.input_uri.value();

    let download_text = vec![
        Line::styled(
            content.load(language),
            Style::default().fg(Color::DarkGray),
        ),
        Line::styled(input_val.to_string(), Style::default().fg(Color::White)),
    ];

    let download_paragraph = Paragraph::new(download_text)
        .block(block_msg)
        .wrap(Wrap { trim: false });

    frame.render_widget(download_paragraph, upper_l);

    let loading = throbber_widgets_tui::Throbber::default()
        .label(loading.load(language))
        .style(Style::default().fg(Color::Cyan))
        .throbber_style(
            Style::default()
                .fg(Color::Red)
                .add_modifier(ratatui::style::Modifier::BOLD),
        )
        .throbber_set(throbber_widgets_tui::HORIZONTAL_BLOCK)
        .use_type(throbber_widgets_tui::WhichUse::Full);

    frame.render_widget(loading, bottom_l);
}
