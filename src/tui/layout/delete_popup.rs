use ratatui::{
    style::{Color, Style},
    text::Text,
    layout::Alignment,
    widgets::{Block, BorderType, Paragraph, Wrap, block::Title},
};

use crate::{l10n::L10N, tui::app::AppTui};

pub fn delete_popup(app: &AppTui) -> Paragraph<'static> {
    let lang = &app.settings.language;
    let input_title = L10N::DeletePopup;
    let title = L10N::DeleteTitle;
    let input_nav = L10N::DeleteContent;

    let block = Block::bordered()
        .title(Title::from(input_title.load(lang)).alignment(Alignment::Right))
        .title(Title::from(title.load(lang)).alignment(Alignment::Left))
        .border_type(BorderType::Rounded)
        .border_style(Style::default().fg(Color::LightRed))
        .style(Style::default().fg(Color::White));

    let text = Text::styled(input_nav.load(lang), Style::default().fg(Color::LightRed));
    let paragraph = Paragraph::new(text)
        .block(block)
        .centered()
        .wrap(Wrap { trim: true });

    paragraph

    // let lang = &app.settings.language;
    // let input_title = L10N::ExitTitle;
    // let input_nav = L10N::ExitContent;
    //
    // let block = Block::bordered()
    //     .border_type(BorderType::Rounded)
    //     .title(input_title.load(lang))
    //     .border_style(Style::default().fg(Color::DarkGray));
    //
    // // let area = centered_rect(60, 20, f.size());
    //
    // let exit_text = Text::styled(
    //     input_nav.load(lang),
    //     Style::default().fg(Color::LightRed),
    // );
    //
    // let exit_paragraph = Paragraph::new(exit_text)
    //     .block(block)
    //     .wrap(Wrap { trim: false });
    //
    // exit_paragraph
}
