use ratatui::{
    style::{Color, Style},
    text::Text,
    layout::Alignment,
    widgets::{Block, BorderType, Paragraph, Wrap, block::Title},
};

use crate::{l10n::L10N, tui::app::AppTui};

pub fn exit_popup(app: &AppTui) -> Paragraph<'static> {
    let lang = &app.settings.language;

    let input_title = L10N::ExitTitle;
    let quite_title = L10N::QuitTitle;
    let input_nav = L10N::ExitContent;

    let block = Block::bordered()
        .border_type(BorderType::Rounded)
        .title(Title::from(input_title.load(lang)).alignment(Alignment::Right))
        .title(Title::from(quite_title.load(lang)).alignment(Alignment::Left))
        .border_style(Style::default().fg(Color::LightYellow));

    // let area = centered_rect(60, 20, f.size());

    let exit_text = Text::styled(
        input_nav.load(lang),
        Style::default().fg(Color::LightRed),
    );

    let exit_paragraph = Paragraph::new(exit_text)
        .block(block)
        .centered()
        .wrap(Wrap { trim: true });

    exit_paragraph
}