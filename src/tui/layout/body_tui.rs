use ratatui::{
    // Frame,
    layout::Alignment,
    style::{Color, Style},
    text::Text,
    widgets::{
        Block,
        block::Title,
        Borders,
        BorderType,
        // Clear,
        Paragraph,
        Wrap,
    }};
use ratatui::layout::{Constraint, Layout, Rect};

use crate::{
    l10n::L10N,
    tui::app::{AppTui, CurrentScreen},
};

pub fn input_editing(app: &AppTui, width: u16) -> Paragraph<'static> {
    let scroll_input = app.input_uri.visual_scroll(width as usize);
    let lang = &app.settings.language;

    let input_title = L10N::InputTitle;
    let input_nav = L10N::InputNavigation;

    let title_nav = match app.current_screen {
        CurrentScreen::Editing => input_nav.load(lang),
        _ => "".to_string(),
    };

    let input_components = Block::default()
        .title(input_title.load(lang))
        .title(Title::from(title_nav).alignment(Alignment::Right))
        .borders(Borders::ALL)
        .border_style(match app.current_screen {
            CurrentScreen::Editing => Style::default().fg(Color::LightYellow),
            _ => Style::default(),
        })
        .border_type(match app.current_screen {
            CurrentScreen::Editing => BorderType::Rounded,
            _ => BorderType::Rounded,
        });

    let value = app.input_uri.value();

    let input_par = Paragraph::new(value.to_string())
        .block(input_components)
        .scroll((0, scroll_input as u16));

    input_par
}

// fn centered_rect(percent_x: u16, percent_y: u16, r: Rect) -> Rect {
//     let popup_layout = Layout::vertical([
//         Constraint::Percentage((100 - percent_y) / 2),
//         Constraint::Percentage(percent_y),
//         Constraint::Percentage((100 - percent_y) / 2),
//     ])
//         .split(r);
//
//     Layout::horizontal([
//         Constraint::Percentage((100 - percent_x) / 2),
//         Constraint::Percentage(percent_x),
//         Constraint::Percentage((100 - percent_x) / 2),
//     ])
//         .split(popup_layout[1])[1]
// }
