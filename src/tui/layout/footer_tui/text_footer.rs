use Language::*;
use TextFooter::*;

use crate::config::Language;

pub enum TextFooter {
    SpanEditing,
    NoteMain,
    NoteEditing,
    NoteSetting,
    NoteBase,
}

impl TextFooter {
    pub fn load_text(&self, language: &Language) -> String {
        let text = match self {
            SpanEditing => match language {
                English => "Press (TAB) To Switch Pane",
            },
            NoteMain => match language {
                English => " (Q) To quit | (TAB) To switch | (SPACE) to select URL ",
            },
            NoteEditing => match language {
                English => " (ESC) To cancel | (TAB) To switch boxes | (ENTER) to complete ",
            },
            NoteSetting => match language {
                English => " (Q) To quit | (TAB) To switch boxes ",
            },
            NoteBase => match language {
                English => " (Q) To quit ",
            },
        };

        text.to_owned()
    }
}
