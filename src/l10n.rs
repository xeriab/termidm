use Language::*;

use crate::config::Language;

#[derive(Copy, Debug, Clone)]
pub enum L10N {
    ConfigsContentChunk,
    ConfigsContentConcurrent,
    ConfigsContentFolder,
    ConfigsContentMinimum,
    DeleteContent,
    DeletePopup,
    DeleteTitle,
    DownloadContent,
    DownloadLoading,
    DownloadNav,
    DownloadPrepareTitle,
    DownloadTitle,
    ErrorPopup,
    ExitContent,
    ExitTitle,
    HistoryTitle,
    InputNavigation,
    InputTitle,
    InputUrlMode,
    QuitTitle,
    SettingsTitle,
    TableNav,
    TabsContentEditing,
    TabsContentLang,
    TabsContentNormal,
    TabsInputTitle,
}

impl L10N {
    pub fn load(&self, language: &Language) -> String {
        let text = match self {
            Self::InputTitle => match language {
                English => " Input URL ",
            },
            Self::InputNavigation => match language {
                English => " Press (ENTER) to download | (ESC) to cancel ",
            },
            Self::ExitTitle => match language {
                English => " Press Y to exit | N to cancel ",
            },
            Self::DeleteTitle => match language {
                English => " Delete ",
            },
            Self::QuitTitle => match language {
                English => " Exit ",
            },
            Self::DeletePopup => match language {
                English => " Press Y to delete | N to cancel ",
            },
            Self::DeleteContent => match language {
                English => "Do you really want to delete the selected download? (y/n)",
            },
            Self::ExitContent => match language {
                English => "Do you really want to exit? (y/n)",
            },
            Self::ConfigsContentFolder => match language {
                English => "Default Folder (absolute path): ",
            },
            Self::ConfigsContentConcurrent => match language {
                English => "Number of Concurrent Downloads: ",
            },
            Self::ConfigsContentChunk => match language {
                English => "Total Chunks Per Download File: ",
            },
            Self::ConfigsContentMinimum => match language {
                English => "Minimum File Size For Concurrent Download (in Kb)",
            },
            Self::TabsInputTitle => match language {
                English => " Enter new value ",
            },
            Self::TabsContentLang => match language {
                English => " ◄ ► to change tab | ▲ ▼  to select language | Press (ENTER) to confirm ",
            },
            Self::TabsContentNormal => match language {
                English => " ◄ ► to change tab | Press (E) to edit ",
            },
            Self::TabsContentEditing => match language {
                English => " Press (ESC) to cancel | (ENTER) to confirm ",
            },
            Self::ErrorPopup => match language {
                English => " Press (ENTER) to continue ",
            },
            Self::TableNav => match language {
                English => " ▲ ▼  To scroll | Press (S) to select | Press (R) to restart | Press (SPACE) to pause/resume | Press (DEL) to delete ",
            },
            Self::DownloadTitle => match language {
                English => " Begin Download ",
            },
            Self::DownloadNav => match language {
                English => " Press Y to download | N to cancel ",
            },
            Self::DownloadContent => match language {
                English => "Would you like download from this url? (y/n): ",
            },
            Self::DownloadPrepareTitle => match language {
                English => "Begin Download",
            },
            Self::DownloadLoading => match language {
                English => "Preparing to download, please wait...",
            },
            Self::SettingsTitle => match language {
                English => " Settings ",
            },
            Self::HistoryTitle => match language {
                English => " History ",
            },
            Self::InputUrlMode => match language {
                English => " Input URL Mode ",
            },
        };

        text.to_owned()
    }
}
