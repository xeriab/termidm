use core::convert::TryFrom;
use core::fmt::Display;
pub use core::fmt::Formatter as Fmt;
use std::{fs, path::PathBuf, sync::Arc};

use eyre::OptionExt;
use futures::stream::{self, StreamExt};
use indicatif::{
    MultiProgress,
    ProgressBar,
    ProgressDrawTarget,
    ProgressStyle,
};
use reqwest::{
    Client,
    header::{
        ACCEPT_RANGES,
        CONTENT_DISPOSITION,
        CONTENT_LENGTH,
        HeaderMap,
        HeaderValue,
        IntoHeaderName,
        RANGE,
    },
    StatusCode,
    Url,
};
use reqwest_middleware::{ClientBuilder, ClientWithMiddleware};
use reqwest_retry::{policies::ExponentialBackoff, RetryTransientMiddleware};
use serde::{Deserialize, Serialize};
use tokio::{fs::OpenOptions, io::AsyncWriteExt};

use crate::utils::{create_range, merge_file};
pub use crate::error::Error;
use crate::tui::app::AppTui;

// use tracing::debug;

//region Constants
//

pub static APP_NAME: &str = "TermiDM";
pub static APP_TITLE: &str = "Internet Download Manager";
pub static APP_VERSION: &str = "0.1.0";
pub static SUB_TITLE: &str = "Challenge #2";

//
//endregion Constants

//region Types
//

/// Type alias for `eyre::Result<T, Report>`
///
/// Usually I use the standard [`Result`] but I found that [`eyre`] is a bit useful.
///
/// `eyre::Result` may be used with one *or* two type parameters.
///
/// ```rust
/// # use termidm::prelude::Result;
///
/// # const IGNORE: &str = stringify! {
/// fn demo1() -> Result<T> {...}
///            // ^ equivalent to std::result::Result<T, eyre::Report>
///
/// fn demo2() -> Result<T, OtherError> {...}
///            // ^ equivalent to std::result::Result<T, OtherError>
/// # };
/// ```
pub type Result<T, E = eyre::Report> = eyre::Result<T, E>;

//
//endregion Types

//region Enumerations
//

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Default)]
pub enum DownloadState {
    Canceled,
    Completed,
    Deleted,
    Downloading,
    Merging,
    Paused,
    #[default]
    Ready,
}

impl Display for DownloadState {
    fn fmt(&self, f: &mut Fmt<'_>) -> std::fmt::Result {
        let rv = match self {
            DownloadState::Canceled => String::from("CANCELED"),
            DownloadState::Completed => String::from("COMPLETED"),
            DownloadState::Deleted => String::from("DELETED"),
            DownloadState::Downloading => String::from("DOWNLOADING"),
            DownloadState::Merging => String::from("MERGING"),
            DownloadState::Paused => String::from("PAUSED"),
            DownloadState::Ready => String::from("READY"),
        };

        write!(f, "{}", rv)
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq, Default)]
pub enum DownloadStatus {
    Fail(String),
    NotStarted,
    Skipped(String),
    Success,
    #[default]
    None,
}


//
//endregion Enumerations

//region Structures
//

/// This structure holds the Download history, so it can be serialised using [`serde`].
#[derive(Debug, Serialize, Deserialize, Clone, Default)]
pub struct DownloadHistory {
    file_name: String,
    is_resumable: bool,
    pub state: DownloadState,
    sizes: u64,
    total_chunks: u64,
    url: String,
}

impl DownloadHistory {
    pub async fn new(uri: &str, total_chunks: u64) -> Result<Self> {
        let real_url = Self::get_download_url(uri).await?;
        let header_obj = HeaderObject::new(real_url.clone()).await?;
        let is_resumable = header_obj.is_ranges();
        let file_name = header_obj
            .get_filename()
            .ok_or_eyre("Error: Can't get file_name")?;
        let sizes = header_obj.get_sizes().unwrap_or_else(|_| 0);

        let obj = Self {
            file_name,
            url: uri.to_string(),
            state: DownloadState::Ready,
            is_resumable,
            total_chunks,
            sizes,
        };

        Ok(obj)
    }

    pub fn ref_array(&self) -> [String; 3] {
        [
            self.file_name.clone(),
            self.url.clone(),
            self.state.to_string(),
        ]
    }

    pub fn url(&self) -> &str {
        &self.url
    }

    pub fn state(&self) -> &DownloadState {
        &self.state
    }

    pub async fn get_download_url(url: &str) -> Result<Url> {
        let res = reqwest::get(url).await?;
        Ok(res.url().to_owned())
    }

    pub async fn get_real_url(&self) -> Result<Url> {
        let res = reqwest::get(self.url()).await?;
        Ok(res.url().to_owned())
    }
}

//region Trauma Downloader
//

/// Represents a file to be downloaded.
#[derive(Debug, Clone)]
pub struct Download {
    /// URL of the file to download.
    pub url: Url,
    /// File name used to save the file on disk.
    pub filename: String,
    pub range_header: Option<(u64, u64)>,
}

impl Download {
    /// Creates a new [`Download`].
    ///
    /// When using the [`Download::try_from`] method, the file name is
    /// automatically extracted from the URL.
    ///
    /// ## Example
    ///
    /// The following calls are equivalent, minus some extra URL validations
    /// performed by `try_from`:
    ///
    /// ```no_run
    /// # use color_eyre::{eyre::Report, Result};
    /// use trauma::download::Download;
    /// use reqwest::Url;
    ///
    /// # fn main() -> Result<(), Report> {
    /// Download::try_from("https://example.com/file-0.1.2.zip")?;
    /// Download::new(&Url::parse("https://example.com/file-0.1.2.zip")?, "file-0.1.2.zip");
    /// # Ok(())
    /// # }
    /// ```
    pub fn new(url: &Url, filename: &str, range: Option<(u64, u64)>) -> Self {
        Self {
            url: url.clone(),
            filename: String::from(filename),
            range_header: range,
        }
    }

    /// Check whether the download is resumable.
    pub async fn is_resumable(
        &self,
        client: &ClientWithMiddleware,
    ) -> Result<bool, reqwest_middleware::Error> {
        let res = client.head(self.url.clone()).send().await?;
        let headers = res.headers();

        match headers.get(ACCEPT_RANGES) {
            None => Ok(false),
            Some(x) if x == "none" => Ok(false),
            Some(_) => Ok(true),
        }
    }

    /// Retrieve the content_length of the download.
    ///
    /// Returns None if the "content-length" header is missing or if its value
    /// is not an u64.
    pub async fn content_length(
        &self,
        client: &ClientWithMiddleware,
    ) -> Result<Option<u64>, reqwest_middleware::Error> {
        if let Some(range) = self.range_header {
            let len = (range.1 - range.0) + 1;
            Ok(Some(len))
        } else {
            let res = client.head(self.url.clone()).send().await?;
            let headers = res.headers();
            match headers.get(CONTENT_LENGTH) {
                None => Ok(None),
                Some(header_value) => match header_value.to_str() {
                    Ok(v) => match v.to_string().parse::<u64>() {
                        Ok(v) => Ok(Some(v)),
                        Err(_) => Ok(None),
                    },
                    Err(_) => Ok(None),
                },
            }
        }
    }
}

impl TryFrom<&Url> for Download {
    type Error = Error;

    fn try_from(value: &Url) -> Result<Self, Self::Error> {
        value
            .path_segments()
            .ok_or_else(|| {
                Error::InvalidUrl(format!(
                    "the url \"{}\" does not contain a valid path",
                    value
                ))
            })?
            .last()
            .map(String::from)
            .map(|filename| Download {
                url: value.clone(),
                filename: form_urlencoded::parse(filename.as_bytes())
                    .map(|(key, val)| [key, val].concat())
                    .collect(),
                range_header: None,
            })
            .ok_or_else(|| {
                Error::InvalidUrl(format!("the url \"{}\" does not contain a filename", value))
            })
    }
}

impl TryFrom<&str> for Download {
    type Error = Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Url::parse(value)
            .map_err(|e| {
                Error::InvalidUrl(format!("the url \"{}\" cannot be parsed: {}", value, e))
            })
            .and_then(|u| Download::try_from(&u))
    }
}

// pub #[derive(Debug)]
// struct DownloadSummary {
//     status_code: StatusCode,
//     size: u64,
//     status: DownloadStatus,
//     resumable: bool,
// }

/// Represents a [`Download`] summary.
#[derive(Debug, Clone)]
pub struct Summary {
    /// Downloaded items.
    download: Download,
    /// HTTP status code.
    status_code: StatusCode,
    /// Download size in bytes.
    size: u64,
    /// DownloadStatus.
    status: DownloadStatus,
    /// Resumable.
    resumable: bool,
}

impl Summary {
    /// Create a new [`Download`] [`Summary`].
    pub fn new(download: Download, status_code: StatusCode, size: u64, resumable: bool) -> Self {
        Self {
            download,
            status_code,
            size,
            status: DownloadStatus::NotStarted,
            resumable,
        }
    }

    /// Attach a status to a [`Download`] [`Summary`].
    pub fn with_status(self, status: DownloadStatus) -> Self {
        Self { status, ..self }
    }

    /// Get the summary's status.
    pub fn status_code(&self) -> StatusCode {
        self.status_code
    }

    /// Get the summary's size.
    pub fn size(&self) -> u64 {
        self.size
    }

    /// Get a reference to the summary's download.
    pub fn download(&self) -> &Download {
        &self.download
    }

    /// Get a reference to the summary's status.
    pub fn status(&self) -> &DownloadStatus {
        &self.status
    }

    pub fn fail(self, msg: impl Display) -> Self {
        Self {
            status: DownloadStatus::Fail(format!("{}", msg)),
            ..self
        }
    }

    /// Set the summary's resumable.
    pub fn set_resumable(&mut self, resumable: bool) {
        self.resumable = resumable;
    }

    /// Get the summary's resumable.
    #[must_use]
    pub fn resumable(&self) -> bool {
        self.resumable
    }
}

/// Represents the download controller.
///
/// A downloader can be created via its builder:
///
/// ```rust
/// # fn main()  {
/// use trauma::downloader::DownloaderBuilder;
///
/// let d = DownloaderBuilder::new().build();
/// # }
/// ```
#[derive(Debug, Clone)]
pub struct Downloader {
    /// Directory where to store the downloaded files.
    directory: PathBuf,
    /// Number of retries per downloaded file.
    retries: u32,
    /// Number of maximum concurrent downloads.
    concurrent_downloads: usize,
    /// Downloader style options.
    style_options: StyleOptions,
    /// Resume the download if necessary and possible.
    resumable: bool,
    /// Custom HTTP headers.
    headers: Option<HeaderMap>,
}

impl Downloader {
    const DEFAULT_RETRIES: u32 = 3;
    const DEFAULT_CONCURRENT_DOWNLOADS: usize = 32;

    /// Starts the downloads.
    pub async fn download(&self, downloads: &[Download]) -> Vec<Summary> {
        self.download_inner(downloads, None).await
    }

    /// Starts the downloads with proxy.
    pub async fn download_with_proxy(
        &self,
        downloads: &[Download],
        proxy: reqwest::Proxy,
    ) -> Vec<Summary> {
        self.download_inner(downloads, Some(proxy)).await
    }

    /// Starts the downloads.
    pub async fn download_inner(
        &self,
        downloads: &[Download],
        proxy: Option<reqwest::Proxy>,
    ) -> Vec<Summary> {
        // Prepare the HTTP client.
        let retry_policy = ExponentialBackoff::builder().build_with_max_retries(self.retries);

        let mut inner_client_builder = Client::builder();
        if let Some(proxy) = proxy {
            inner_client_builder = inner_client_builder.proxy(proxy);
        }
        if let Some(headers) = &self.headers {
            inner_client_builder = inner_client_builder.default_headers(headers.clone());
        }

        let inner_client = inner_client_builder.build().unwrap();

        let client = ClientBuilder::new(inner_client)
            // Trace HTTP requests. See the tracing crate to make use of these traces.
            // Retry failed requests.
            .with(RetryTransientMiddleware::new_with_policy(retry_policy))
            .build();

        // Prepare the progress bar.
        let multi = match self.style_options.clone().is_enabled() {
            true => Arc::new(MultiProgress::new()),
            false => Arc::new(MultiProgress::with_draw_target(ProgressDrawTarget::hidden())),
        };
        let main = Arc::new(
            multi.add(
                self.style_options
                    .main
                    .clone()
                    .to_progress_bar(downloads.len() as u64),
            ),
        );
        main.tick();

        // Download the files asynchronously.
        let summaries = stream::iter(downloads)
            .map(|d| self.fetch(&client, d, multi.clone(), main.clone()))
            .buffer_unordered(self.concurrent_downloads)
            .collect::<Vec<_>>()
            .await;

        // Finish the progress bar.
        if self.style_options.main.clear {
            main.finish_and_clear();
        } else {
            main.finish();
        }

        // Return the download summaries.
        summaries
    }

    /// Fetches the files and write them to disk.
    async fn fetch(
        &self,
        client: &ClientWithMiddleware,
        download: &Download,
        multi: Arc<MultiProgress>,
        main: Arc<ProgressBar>,
    ) -> Summary {
        // Create a download summary.
        let mut size_on_disk: u64 = 0;
        let mut can_resume = false;
        let output = self.directory.join(&download.filename);
        let mut summary = Summary::new(
            download.clone(),
            StatusCode::BAD_REQUEST,
            size_on_disk,
            can_resume,
        );
        let mut content_length: Option<u64> = None;

        // If resumable is turned on...
        if self.resumable {
            can_resume = match download.is_resumable(client).await {
                Ok(r) => r,
                Err(e) => {
                    return summary.fail(e);
                }
            };

            // Check if there is a file on disk already.
            if can_resume && output.exists() {
                // debug!("A file with the same name already exists at the destination.");
                // If so, check file length to know where to restart the download from.
                size_on_disk = match output.metadata() {
                    Ok(m) => m.len(),
                    Err(e) => {
                        return summary.fail(e);
                    }
                };

                // Retrieve the download size from the header if possible.
                content_length = match download.content_length(client).await {
                    Ok(l) => l,
                    Err(e) => {
                        return summary.fail(e);
                    }
                };
            }

            // Update the summary accordingly.
            summary.set_resumable(can_resume);
        }

        // If resumable is turned on...
        // Request the file.
        // debug!("Fetching {}", &download.url);
        let mut req = client.get(download.url.clone());

        if let Some(range_str) = download.range_header {
            if can_resume {
                let start = size_on_disk + range_str.0;
                let end = range_str.1;
                req = req.header(RANGE, format!("bytes={start}-{end}"));
            } else {
                req = req.header(RANGE, format!("bytes={}-{}", range_str.0, range_str.1));
            }
        } else if self.resumable && can_resume {
            req = req.header(RANGE, format!("bytes={}-", size_on_disk));
        }

        // Add extra headers if needed.
        if let Some(ref h) = self.headers {
            req = req.headers(h.to_owned());
        }

        // Ensure there was no error while sending the request.
        let res = match req.send().await {
            Ok(res) => res,
            Err(e) => {
                return summary.fail(e);
            }
        };

        // Checks whether we need to download the file.
        if let Some(content_length) = content_length {
            if content_length == size_on_disk {
                main.inc(1);
                return summary.with_status(DownloadStatus::Skipped(
                    "the file was already fully downloaded".into(),
                ));
            }
        }

        // Check the status for errors.
        match res.error_for_status_ref() {
            Ok(_res) => (),
            Err(e) => return summary.fail(e),
        };

        // Update the summary with the collected details.
        let size = content_length.unwrap_or_default() + size_on_disk;
        let status = res.status();
        summary = Summary::new(download.clone(), status, size, can_resume);

        // If there is nothing else to download for this file, we can return.
        if size_on_disk > 0 && size == size_on_disk {
            main.inc(1);
            return summary.with_status(DownloadStatus::Skipped(
                "the file was already fully downloaded".into(),
            ));
        }

        // Create the progress bar.
        // If the download is being resumed, the progress bar position is
        // updated to start where the download stopped before.
        let pb = multi.add(
            self.style_options
                .child
                .clone()
                .to_progress_bar(size)
                .with_position(size_on_disk),
        );

        // Prepare the destination directory/file.
        let output_dir = output.parent().unwrap_or(&output);
        // debug!("Creating destination directory {:?}", output_dir);
        match fs::create_dir_all(output_dir) {
            Ok(_res) => (),
            Err(e) => {
                return summary.fail(e);
            }
        };

        // debug!("Creating destination file {:?}", &output);
        let mut file = match OpenOptions::new()
            .create(true)
            .write(true)
            .append(can_resume)
            .open(output)
            .await
        {
            Ok(file) => file,
            Err(e) => {
                return summary.fail(e);
            }
        };

        let mut final_size = size_on_disk;

        // Download the file chunk by chunk.
        // debug!("Retrieving chunks...");
        let mut stream = res.bytes_stream();
        while let Some(item) = stream.next().await {
            // Retrieve chunk.
            let mut chunk = match item {
                Ok(chunk) => chunk,
                Err(e) => {
                    return summary.fail(e);
                }
            };
            let chunk_size = chunk.len() as u64;
            final_size += chunk_size;
            pb.inc(chunk_size);

            // Write the chunk to disk.
            match file.write_all_buf(&mut chunk).await {
                Ok(_res) => (),
                Err(e) => {
                    return summary.fail(e);
                }
            };
        }

        // Finish the progress bar once complete, and optionally remove it.
        if self.style_options.child.clear {
            pb.finish_and_clear();
        } else {
            pb.finish();
        }

        // Advance the main progress bar.
        main.inc(1);

        // Create a new summary with the real download size
        let summary = Summary::new(download.clone(), status, final_size, can_resume);
        // Return the download summary.
        summary.with_status(DownloadStatus::Success)
    }
}

/// A builder used to create a [`Downloader`].
///
/// ```
/// # fn main()  {
/// use trauma::downloader::DownloaderBuilder;
///
/// let d = DownloaderBuilder::new().retries(5).directory("downloads".into()).build();
/// # }
/// ```
pub struct DownloaderBuilder(Downloader);

impl DownloaderBuilder {
    /// Creates a builder with the default options.
    pub fn new() -> Self {
        DownloaderBuilder::default()
    }

    /// Convenience function to hide the progress bars.
    pub fn hidden() -> Self {
        let d = DownloaderBuilder::default();
        d.style_options(StyleOptions::new(
            ProgressBarOpts::hidden(),
            ProgressBarOpts::hidden(),
        ))
    }

    /// Sets the directory where to store the [`Download`]s.
    pub fn directory(mut self, directory: PathBuf) -> Self {
        self.0.directory = directory;
        self
    }

    /// Set the number of retries per [`Download`].
    pub fn retries(mut self, retries: u32) -> Self {
        self.0.retries = retries;
        self
    }

    /// Set the number of concurrent [`Download`]s.
    pub fn concurrent_downloads(mut self, concurrent_downloads: usize) -> Self {
        self.0.concurrent_downloads = concurrent_downloads;
        self
    }

    /// Set the downloader style options.
    pub fn style_options(mut self, style_options: StyleOptions) -> Self {
        self.0.style_options = style_options;
        self
    }

    fn new_header(&self) -> HeaderMap {
        match self.0.headers {
            Some(ref h) => h.to_owned(),
            _ => HeaderMap::new(),
        }
    }

    pub fn headers(mut self, headers: HeaderMap) -> Self {
        let mut new = self.new_header();
        new.extend(headers);

        self.0.headers = Some(new);
        self
    }

    pub fn header<K: IntoHeaderName>(mut self, name: K, value: HeaderValue) -> Self {
        let mut new = self.new_header();

        new.insert(name, value);

        self.0.headers = Some(new);
        self
    }

    /// Create the [`Downloader`] with the specified options.
    pub fn build(self) -> Downloader {
        Downloader {
            directory: self.0.directory,
            retries: self.0.retries,
            concurrent_downloads: self.0.concurrent_downloads,
            style_options: self.0.style_options,
            resumable: self.0.resumable,
            headers: self.0.headers,
        }
    }
}

impl Default for DownloaderBuilder {
    fn default() -> Self {
        Self(Downloader {
            directory: std::env::current_dir().unwrap_or_default(),
            retries: Downloader::DEFAULT_RETRIES,
            concurrent_downloads: Downloader::DEFAULT_CONCURRENT_DOWNLOADS,
            style_options: StyleOptions::default(),
            resumable: true,
            headers: None,
        })
    }
}

/// Define the [`Downloader`] options.
///
/// By default, the main progress bar will stay on the screen upon completion,
/// but the child ones will be cleared once complete.
#[derive(Debug, Clone)]
pub struct StyleOptions {
    /// Style options for the main progress bar.
    main: ProgressBarOpts,
    /// Style options for the child progress bar(s).
    child: ProgressBarOpts,
}

impl Default for StyleOptions {
    fn default() -> Self {
        Self {
            main: ProgressBarOpts {
                template: Some(ProgressBarOpts::TEMPLATE_BAR_WITH_POSITION.into()),
                progress_chars: Some(ProgressBarOpts::CHARS_FINE.into()),
                enabled: true,
                clear: false,
            },
            child: ProgressBarOpts::with_pip_style(),
        }
    }
}

impl StyleOptions {
    /// Create new [`Downloader`] [`StyleOptions`].
    pub fn new(main: ProgressBarOpts, child: ProgressBarOpts) -> Self {
        Self { main, child }
    }

    /// Set the options for the main progress bar.
    pub fn set_main(&mut self, main: ProgressBarOpts) {
        self.main = main;
    }

    /// Set the options for the child progress bar.
    pub fn set_child(&mut self, child: ProgressBarOpts) {
        self.child = child;
    }

    /// Return `false` if neither the main nor the child bar is enabled.
    pub fn is_enabled(&self) -> bool {
        self.main.enabled || self.child.enabled
    }
}

/// Define the options for a progress bar.
#[derive(Debug, Clone)]
pub struct ProgressBarOpts {
    /// Progress bar template string.
    template: Option<String>,
    /// Progression characters set.
    ///
    /// There must be at least 3 characters for the following states:
    /// "filled", "current", and "to do".
    progress_chars: Option<String>,
    /// Enable or disable the progress bar.
    enabled: bool,
    /// Clear the progress bar once completed.
    clear: bool,
}

impl Default for ProgressBarOpts {
    fn default() -> Self {
        Self {
            template: None,
            progress_chars: None,
            enabled: true,
            clear: true,
        }
    }
}

// NOTE: I stole the code from a project I actually made using golang and bash
impl ProgressBarOpts {
    /// Template representing the bar and its position.
    ///
    ///`███████████████████████████████████████ 11/12 (99%) eta 00:00:02`
    pub const TEMPLATE_BAR_WITH_POSITION: &'static str =
        "{bar:40.blue} {pos:>}/{len} ({percent}%) eta {eta_precise:.blue}";
    /// Template which looks like the Python package installer pip.
    ///
    /// `━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 211.23 KiB/211.23 KiB 1008.31 KiB/s eta 0s`
    pub const TEMPLATE_PIP: &'static str =
        "{bar:40.green/black} {bytes:>11.green}/{total_bytes:<11.green} {bytes_per_sec:>13.red} eta {eta:.blue}";
    /// Use increasing quarter blocks as progress characters: `"█▛▌▖  "`.
    pub const CHARS_BLOCKY: &'static str = "█▛▌▖  ";
    /// Use fade-in blocks as progress characters: `"█▓▒░  "`.
    pub const CHARS_FADE_IN: &'static str = "█▓▒░  ";
    /// Use fine blocks as progress characters: `"█▉▊▋▌▍▎▏  "`.
    pub const CHARS_FINE: &'static str = "█▉▊▋▌▍▎▏  ";
    /// Use a line as progress characters: `"━╾─"`.
    pub const CHARS_LINE: &'static str = "━╾╴─";
    /// Use rough blocks as progress characters: `"█  "`.
    pub const CHARS_ROUGH: &'static str = "█  ";
    /// Use increasing height blocks as progress characters: `"█▇▆▅▄▃▂▁  "`.
    pub const CHARS_VERTICAL: &'static str = "█▇▆▅▄▃▂▁  ";

    /// Create a new [`ProgressBarOpts`].
    pub fn new(
        template: Option<String>,
        progress_chars: Option<String>,
        enabled: bool,
        clear: bool,
    ) -> Self {
        Self {
            template,
            progress_chars,
            enabled,
            clear,
        }
    }

    /// Create a [`ProgressStyle`] based on the provided options.
    pub fn to_progress_style(self) -> ProgressStyle {
        let mut style = ProgressStyle::default_bar();
        if let Some(template) = self.template {
            style = style.template(&template).unwrap();
        }
        if let Some(progress_chars) = self.progress_chars {
            style = style.progress_chars(&progress_chars);
        }
        style
    }

    /// Create a [`ProgressBar`] based on the provided options.
    pub fn to_progress_bar(self, len: u64) -> ProgressBar {
        // Return a hidden Progress bar if we disabled it.
        if !self.enabled {
            return ProgressBar::hidden();
        }

        // Otherwise returns a ProgressBar with the style.
        let style = self.to_progress_style();
        ProgressBar::new(len).with_style(style)
    }

    /// Create a new [`ProgressBarOpts`] which looks like Python pip.
    pub fn with_pip_style() -> Self {
        Self {
            template: Some(ProgressBarOpts::TEMPLATE_PIP.into()),
            progress_chars: Some(ProgressBarOpts::CHARS_LINE.into()),
            enabled: true,
            clear: true,
        }
    }

    /// Set to `true` to clear the progress bar upon completion.
    pub fn set_clear(&mut self, clear: bool) {
        self.clear = clear;
    }

    /// Create a new [`ProgressBarOpts`] which hides the progress bars.
    pub fn hidden() -> Self {
        Self {
            enabled: false,
            ..ProgressBarOpts::default()
        }
    }
}

//
//endregion Trauma Downloader

//region Request
//

pub struct HeaderObject {
    header: HeaderMap,
    url: Url,
}

impl HeaderObject {
    pub async fn new(uri: Url) -> Result<Self> {
        let resp = Client::new().get(uri.clone()).send().await?;
        dbg!(resp.headers());
        Ok(HeaderObject {
            header: resp.headers().clone(),
            url: uri,
        })
    }

    pub fn get_sizes(&self) -> Result<u64> {
        let sizes = self.header.get(CONTENT_LENGTH).ok_or_eyre("Size Unknown")?;
        let size_num = sizes.to_str()?.parse::<u64>()?;
        Ok(size_num)
    }

    pub fn is_ranges(&self) -> bool {
        let accept_range = match self.header.get(ACCEPT_RANGES) {
            Some(x) => x != "none",
            None => false,
        };
        accept_range
    }

    pub fn get_filename(&self) -> Option<String> {
        let name: Option<String> = self
            .header
            .get(CONTENT_DISPOSITION)
            .and_then(|disposition| {
                disposition
                    .to_str()
                    .ok()
                    .filter(|content| content.contains("filename"))
            })
            .map(Into::into);
        match name {
            Some(name) => {
                match name
                    .split(';')
                    .find(|part| part.trim_start().starts_with("filename="))
                    .and_then(|filename_part| filename_part.split('=').nth(1))
                    .and_then(|filename| Some(filename.trim_matches('"')))
                {
                    Some(s) => Some(s.to_owned()),
                    None => None,
                }
            }
            None => {
                let path_segment = self.url.path_segments()?;

                let filename = path_segment.last().map(String::from).map(|f| {
                    let file_name = form_urlencoded::parse(f.as_bytes())
                        .map(|(key, val)| [key, val].concat())
                        .collect();
                    file_name
                });

                filename
            }
        }
    }

    pub fn get_url(&self) -> &Url {
        &self.url
    }
}

//
//endregion Request

//
//endregion Structures

//region Public Routines
//

pub async fn start_download(
    temp: PathBuf,
    uri: &Url,
    range_header: &[(u64, u64)],
    num_concur: usize,
) -> Result<Vec<Summary>> {
    tokio::fs::create_dir_all(&temp).await?;

    let mut batch_dl = Vec::with_capacity(range_header.len());

    for (i, range) in range_header.iter().enumerate() {
        let dl = Download::new(uri, &i.to_string(), Some(*range));
        batch_dl.push(dl);
    }

    let begin = DownloaderBuilder::new()
        .directory(temp)
        .concurrent_downloads(num_concur)
        .build();

    let summary = begin.download(&batch_dl).await;

    Ok(summary)
}

pub async fn download_chunk(app: &mut AppTui, key: u32) -> Result<()> {
    let history = app.get_history(key)?.clone();
    let url = history.url();
    let is_resumable = &history.is_resumable;

    println!("Begin Download : {url}");

    let file_name = &history.file_name;

    let real_url = history.get_real_url().await?;
    let download_path = &app.settings.default_folder.clone();

    let min_sz = &app.settings.minimum_size;
    let sizes = &history.sizes;
    let is_min = min_sz * 1000 > *sizes;

    if !is_resumable || is_min {
        let downloader = vec![Download::new(&real_url, file_name, None)];
        let build = DownloaderBuilder::new()
            .directory(download_path.clone())
            .build();

        build.download(&downloader).await;
        app.update_stage(key, DownloadState::Completed);

        let _ = app.save_history();
    } else {
        let total_chunks = &history.total_chunks;
        let number_concurrent = app.settings.concurrent_downloads;

        let ranges =
            create_range(*sizes, *total_chunks).ok_or_eyre("Error: divisor should be non-zero")?;

        let temp = download_path.join("temp").join(&file_name);

        app.update_stage(key, DownloadState::Downloading);

        let _ = app.save_history();
        let res = start_download(temp.clone(), &real_url, &ranges, number_concurrent).await;

        if let Ok(_) = res {
            app.update_stage(key, DownloadState::Merging);

            let _ = app.save_history();

            merge_file(&temp, ranges.len(), &download_path, &file_name).await?;
            app.update_stage(key, DownloadState::Completed);

            let _ = app.save_history();
        }
    }

    Ok(())
}

//
//endregion Public Routines

#[cfg(test)]
mod test {
    use eyre::eyre;

    use super::*;

    const DOMAIN: &str = "https://github.com/cogu/cfile/archive/refs/heads/master.zip";
    const URI: &str = "https://huggingface.co/datasets/ym0v0my/Time_series_dataset/resolve/main/all_six_datasets.zip?download=true";
    const URI_2: &str = "https://image.civitai.com/xG1nkqKTMzGDvpLrqFT7WA/dcf6f112-d385-4fea-8c08-1aa8457ffec4/transcode=true,width=450/AD_00010.webm";

    #[test]
    fn test_dirs() {
        if let Some(dir) = dirs::home_dir() {
            let down = dir.join("Downloads");
            dbg!(down);
        };

        assert_eq!(true, true);
    }

    async fn get_download_url(url: &str) -> Result<Url> {
        let res = reqwest::get(url).await?;
        Ok(res.url().to_owned())
    }

    #[tokio::test]
    async fn test_concurrent_download() -> Result<()> {
        let download_dirs = dirs::download_dir()
            .or(Some(PathBuf::from("test/Downloads/")))
            .ok_or_eyre("Error : No Download Dir")?;

        let temp = download_dirs.join("temp");

        let url = get_download_url(URI).await?;
        let header_obj = HeaderObject::new(url).await?;
        if !header_obj.is_ranges() {
            return Err(eyre!("Not Resumable"));
        }
        let uri = header_obj.get_url();

        let sizes = header_obj.get_sizes()?;
        dbg!(sizes);
        let divisor = 8;

        let ranges =
            create_range(sizes, divisor).ok_or_eyre("Error: divisor should be non-zero")?;
        let out_file = "output.zip";

        let summary = start_download(temp.clone(), uri, &ranges, 4).await;
        assert!(summary.is_ok());

        let res = merge_file(&temp, divisor as usize, &temp, out_file).await;

        dbg!(&res);
        assert!(res.is_ok());

        Ok(())
    }

    #[tokio::test]
    async fn test_single_download() -> Result<()> {
        let dl = Download::try_from(URI_2);

        assert!(&dl.is_ok());

        let dlr = vec![dl.expect("get dl")];
        let build = DownloaderBuilder::new()
            .directory(PathBuf::from("test/Downloads/"))
            .build();

        let arr = build.download(&dlr).await;

        // dbg!(arr);

        // assert_eq!(false, true);

        Ok(())
    }

    #[test]
    fn test_try_from_url() {
        let u = Url::parse(DOMAIN).unwrap();
        let d = Download::try_from(&u).unwrap();
        assert_eq!(d.filename, "master.zip")
    }

    #[test]
    fn test_try_from_string() {
        let d = Download::try_from(DOMAIN).unwrap();
        assert_eq!(d.filename, "master.zip")
    }

    #[test]
    fn test_builder_defaults() {
        let d = DownloaderBuilder::new().build();
        assert_eq!(d.retries, Downloader::DEFAULT_RETRIES);
        assert_eq!(
            d.concurrent_downloads,
            Downloader::DEFAULT_CONCURRENT_DOWNLOADS
        );
    }

    async fn get_download_url(url: &str) -> Result<Url> {
        let res = reqwest::get(url).await?;
        dbg!(res.url().as_str());
        Ok(res.url().to_owned())
    }

    #[tokio::test]
    async fn test_get_filename_1() -> Result<()> {
        let url = get_download_url(URI).await?;
        let head = HeaderObject::new(url.clone()).await?;
        let name = head.get_filename();

        let name_str = name.unwrap();

        dbg!(&name_str);

        assert_eq!("all_six_datasets.zip", &name_str);

        Ok(())
    }

    #[tokio::test]
    async fn test_get_filename_2() -> Result<()> {
        let url = get_download_url(DOMAIN).await?;
        let head = HeaderObject::new(url.clone()).await?;
        let name = head.get_filename();

        let name_str = name.unwrap();

        dbg!(&name_str);

        assert_eq!("cfile-main.zip", &name_str);

        Ok(())
    }
}