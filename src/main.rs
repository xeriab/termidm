use std::{io, time::Duration};

use crossterm::{
    event::{DisableMouseCapture, EnableMouseCapture},
    execute,
    terminal::{
        disable_raw_mode,
        enable_raw_mode,
        EnterAlternateScreen,
        LeaveAlternateScreen,
    },
};
use ratatui::{
    backend::CrosstermBackend,
    Terminal,
    widgets::Widget,
};

use crate::config::Config;
pub use crate::prelude::*;
use crate::tui::app::AppTui;
use crate::tui::event as TuiEvent;

pub mod config;
pub mod error;
pub mod prelude;
// pub mod program;
pub mod tui;
pub mod utils;
pub mod l10n;

#[tokio::main]
async fn main() -> Result<()> {
    loop {
        enable_raw_mode()?;
        let mut stderr = io::stderr();

        execute!(stderr, EnterAlternateScreen, EnableMouseCapture)?;

        let mut term = Terminal::new(CrosstermBackend::new(stderr))?;

        let config = Config::new();
        let mut app = AppTui::new(config);

        let res = TuiEvent::run_app(&mut term, &mut app).await;

        disable_raw_mode()?;

        execute!(
            term.backend_mut(),
            LeaveAlternateScreen,
            DisableMouseCapture
        )?;

        term.clear()?;
        term.show_cursor()?;

        match res {
            Ok(do_print) => {
                if do_print {
                    let vec_value = &app.saved_input.clone();

                    if vec_value.is_empty() {
                        println!("empty");
                    } else {
                        for (key_value, _) in vec_value {
                            download_chunk(&mut app, *key_value).await?;
                        }
                    }
                } else {
                    break;
                }
            }
            Err(err) => {
                println!("{}", err.to_string());
                break;
            }
        }

        let duration = Duration::from_secs(1);

        tokio::time::sleep(duration).await;
    }

    Ok(())
}
