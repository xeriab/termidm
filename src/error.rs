#[derive(Debug, Default, thiserror::Error)]
pub enum Error {
    #[error("Generic Error: {0}")]
    Generic(String),
    /// Error from an underlying system.
    #[error("Internal Error: {0}")]
    Internal(String),
    /// Error from the underlying URL parser or the expected URL format.
    #[error("Invalid URL: {0}")]
    InvalidUrl(String),
    /// I/O Error.
    #[error("I/O Error")]
    IO(#[from] std::io::Error),
    /// Error from the Reqwest library.
    #[error("Request Error")]
    // Reqwest { #[from] source: reqwest::Error },
    Reqwest(#[from] reqwest::Error),
    #[error("Unknown Error")]
    #[default]
    Unknown,
}
