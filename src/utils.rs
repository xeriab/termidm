use std::path::{Path, PathBuf};

use tokio::fs;
use tokio::io::AsyncWriteExt;

use crate::prelude::Result;

pub fn create_range(num: u64, divisor: u64) -> Option<Vec<(u64, u64)>> {
    if divisor == 0 {
        return None;
    }

    let range_size = num / divisor;
    let mut ranges = Vec::new();
    let mut start = 0;

    for i in 0..divisor {
        let mut end = (start + range_size) - 1;

        if i == divisor - 1 {
            end = num
        }

        ranges.push((start, end));
        start = end + 1;
    }

    Some(ranges)
}

pub fn string_to_vec(input: &str) -> Vec<String> {
    input.split([',', ' ']).map(|s| s.trim().to_owned()).collect()
}

pub fn get_available_filename(mut filename: PathBuf) -> PathBuf {
    while filename.exists() {
        let name_file = filename.file_stem().unwrap().to_string_lossy();

        let ext = filename.extension().unwrap().to_string_lossy();

        let new_name = if name_file.ends_with(')') {
            let index_split = name_file.rfind('(').unwrap_or(name_file.len());
            let (prefix, suffix) = name_file.split_at(index_split);
            let new_suffix = match suffix
                .trim_start_matches('(')
                .trim_end_matches(')')
                .parse::<u32>()
            {
                Ok(num) => (num + 1).to_string(),
                Err(_) => "1".to_string(),
            };

            format!("{}({})", prefix, new_suffix)
        } else {
            format!("{name_file}(1)")
        };

        let new_filename = format!("{new_name}.{ext}", );

        filename.set_file_name(new_filename);
    }

    filename
}

pub async fn merge_file(
    out_folder: &Path,
    divisor: usize,
    parent: &Path,
    output_name: &str,
) -> Result<()> {
    println!("INFO: Start Merging");
    let mut dir_entry = fs::read_dir(out_folder).await?;

    let mut file_paths = Vec::with_capacity(divisor);

    while let Some(file_chunk) = dir_entry.next_entry().await? {
        let path_file = file_chunk.path();

        if path_file.is_file() {
            let ext = path_file.extension();
            if ext.is_none() {
                file_paths.push(path_file);
            }
        }
    }

    file_paths.sort_by_key(|p| {
        let str_path = p.file_name().and_then(|os_str| os_str.to_str());

        let mut num = 0;

        match str_path {
            Some(file_str) => match file_str.parse::<u32>() {
                Ok(num_res) => num = num_res,
                Err(err) => println!("ERROR: {err}"),
            },
            None => println!("ERROR: failed to conver os str to str"),
        }

        num
    });

    let output_file = parent.join(output_name);
    let final_file = get_available_filename(output_file);

    let mut file_output = fs::OpenOptions::new()
        .append(true)
        .create(true)
        .open(final_file)
        .await?;

    for p in file_paths.iter() {
        let buff = fs::read(p).await?;
        file_output.write_all(&buff).await?;
    }

    let res = fs::remove_dir_all(out_folder).await;

    if res.is_err() {
        println!("{:#?}", res);
    }

    println!("INFO: Done Merging");

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vec_str() {
        let names = "alice,bob foo,bar baz";
        let names_vec: Vec<String> = string_to_vec(names);

        assert_eq!(
            vec![
                "alice".to_owned(),
                "bob".to_owned(),
                "foo".to_owned(),
                "bar".to_owned(),
                "baz".to_owned(),
            ],
            names_vec
        );
    }

    #[test]
    fn test_create_range_normal() {
        assert_eq!(
            create_range(25, 4),
            Some(vec![(0, 5), (6, 11), (12, 17), (18, 25)])
        );
    }

    #[test]
    fn test_create_range_normal_1() {
        assert_eq!(
            create_range(100, 10),
            Some(vec![
                (0, 9),
                (10, 19),
                (20, 29),
                (30, 39),
                (40, 49),
                (50, 59),
                (60, 69),
                (70, 79),
                (80, 89),
                (90, 100),
            ])
        );
    }

    #[test]
    fn test_create_range_normal_2() {
        assert_eq!(
            create_range(50, 7),
            Some(vec![
                (0, 6),
                (7, 13),
                (14, 20),
                (21, 27),
                (28, 34),
                (35, 41),
                (42, 50),
            ])
        );
    }

    #[test]
    fn test_create_range_normal_3() {
        assert_eq!(
            create_range(30, 5),
            Some(vec![(0, 5), (6, 11), (12, 17), (18, 23), (24, 30)])
        );
    }

    #[test]
    fn test_create_range_normal_4() {
        assert_eq!(
            create_range(1000, 8),
            Some(vec![
                (0, 124),
                (125, 249),
                (250, 374),
                (375, 499),
                (500, 624),
                (625, 749),
                (750, 874),
                (875, 1000),
            ])
        );
    }

    #[test]
    fn test_create_range_normal_5() {
        assert_eq!(
            create_range(1500, 8),
            Some(vec![
                (0, 186),
                (187, 373),
                (374, 560),
                (561, 747),
                (748, 934),
                (935, 1121),
                (1122, 1308),
                (1309, 1500),
            ])
        );
    }

    #[test]
    fn test_create_range_normal_6() {
        assert_eq!(
            create_range(2000, 8),
            Some(vec![
                (0, 249),
                (250, 499),
                (500, 749),
                (750, 999),
                (1000, 1249),
                (1250, 1499),
                (1500, 1749),
                (1750, 2000),
            ])
        );
    }

    #[test]
    fn test_create_range_normal_7() {
        assert_eq!(
            create_range(24651, 8),
            Some(vec![
                (0, 3080),
                (3081, 6161),
                (6162, 9242),
                (9243, 12323),
                (12324, 15404),
                (15405, 18485),
                (18486, 21566),
                (21567, 24651),
            ])
        );
    }

    #[test]
    fn test_avail() {
        let filename = PathBuf::from("test/test.txt");
        let avail = get_available_filename(filename);
        let expect_name = PathBuf::from("test/test.txt");

        assert_eq!(expect_name, avail);
    }
}
