" -*- tab-width: 4; encoding: utf-8; mode: vim; -*-
"
" ./.vimrc
"
" Copyright (c) 2024 Xeriab Nabil <xeriab@tuta.io>
"
" SPDX-License-Identifier: MIT
"


" vim: set ts=2 sw=2 tw=80 noet :
