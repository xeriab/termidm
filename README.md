# TermiDM 

Yet another `TUI` internet download application.

# Demo

Key Takeaways:

- [ratatui](https://github.com/ratatui-org/ratatui) and immediate rendering
- Concurrency

## Acknowledgment

I already forked a similar program in the past for my personal use since I'm a 
Linux user/developer who uses tiling window managers, so I rely on using `CLI` 
and `TUI` apps such as `ranger`, `lf`, `fzf` and `vim`. I modified the code with 
the requirements mentioned in the Challenge [#2](). so I'll be lying if I said 
that I started the development from scratch. since the time needed for 
completing this kind of stuff is not enough because I never used `ratatui` or 
some libraries used for downloading files using Rust. I am familiar with such 
tools in other programming languages such as `Bash`, `PHP` and `Python`.